﻿using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SeedScreen : MonoBehaviour
{
    public GameObject inputField, ButtonPlay;
    public Toggle deck1Toggle, deck2Toggle, deck3Toggle, deck4Toggle, deck5Toggle;

    public static String[] cartas; // 526 cartas: 120 deck1, 100 deck2, 74 deck3, 120 deck4, 112 deck5
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if ((deck1Toggle.isOn || deck2Toggle.isOn || deck3Toggle.isOn || deck4Toggle.isOn || deck5Toggle.isOn) && inputField.GetComponent<TMP_InputField>().text.Length == 3)
        {
            ButtonPlay.GetComponent<Button>().interactable = true;
        }
        else
        {
            ButtonPlay.GetComponent<Button>().interactable = false;
        }
    }

    public void CriarBaralho()
    {
        var listaDeCartas = new List<String>();

        if (deck1Toggle.isOn)
        {
            String[] deck1 = new String[120];
            for (int i = 0; i < deck1.Length; i++)
            {
                int value = i + 1;
                String codigoCarta = "A001-" + value.ToString("000");
                listaDeCartas.Add(codigoCarta);
            }
        }
        if (deck2Toggle.isOn)
        {
            String[] deck2 = new String[100];
            for (int i = 0; i < deck2.Length; i++)
            {
                int value = i + 1;
                String codigoCarta = "A002-" + value.ToString("000");
                listaDeCartas.Add(codigoCarta);
            }
        }
        if (deck3Toggle.isOn)
        {
            String[] deck3 = new String[74];
            for (int i = 0; i < deck3.Length; i++)
            {
                int value = i + 1;
                String codigoCarta = "A003-" + value.ToString("000");
                listaDeCartas.Add(codigoCarta);
            }
        }
        if (deck4Toggle.isOn)
        {
            String[] deck4 = new String[120];
            for (int i = 0; i < deck4.Length; i++)
            {
                int value = i + 1;
                String codigoCarta = "A004-" + value.ToString("000");
                listaDeCartas.Add(codigoCarta);
            }
        }
        if (deck5Toggle.isOn)
        {
            String[] deck5 = new String[112];
            for (int i = 0; i < deck5.Length; i++)
            {
                int value = i + 1;
                String codigoCarta = "A005-" + value.ToString("000");
                listaDeCartas.Add(codigoCarta);
            }
        }

        cartas = listaDeCartas.ToArray();

        //cartas = new String[deck1.Length + deck2.Length + deck3.Length + deck4.Length + deck5.Length];
        // deck1.CopyTo(cartas, 0);
        // deck2.CopyTo(cartas, deck1.Length);
        // deck3.CopyTo(cartas, deck1.Length + deck2.Length);
        // deck4.CopyTo(cartas, deck1.Length + deck2.Length + deck3.Length);
        // deck5.CopyTo(cartas, deck1.Length + deck2.Length + deck3.Length + deck4.Length);

        // var tam = deck1.Length + deck2.Length + deck3.Length + deck4.Length + deck5.Length;
        Debug.Log("Quantidade de cartas no baralho:" + cartas.Length);
        // foreach (var item in cartas)
        // {
        //     Debug.Log("Cartas no baralho: " + item);
        // }
    }
    public void EmbaralharBaralho(int seed)
    {
        CriarBaralho();

        UnityEngine.Random.InitState(seed);

        for (int i = (cartas.Length - 1); i > -1; i--)
        {
            int j = UnityEngine.Random.Range(0, cartas.Length);
            String temp = cartas[j];
            cartas[j] = cartas[i];
            cartas[i] = temp;
        }

        PlayerPrefs.SetInt("cartaNumero", 1); //Inicializar a variável para exibir as cartas (vai ser incrementada a cada carta exibida)

        Debug.Log(cartas[0] + "/" + cartas[1] + "/" + cartas[2] + "/" + cartas[3] + "/" + cartas[4] + "/" + cartas[5]);
    }

    // public void VoltarTela()
    // {
    //     SceneManager.LoadScene("TelaMenu");
    // }

    public void AvancarTela()
    {
        if (inputField.GetComponent<TMP_InputField>().text.Length == 3)
        {
            PlayerPrefs.SetInt("seed", int.Parse(inputField.GetComponent<TMP_InputField>().text));

            Debug.Log("Foi usado o seed:" + PlayerPrefs.GetInt("seed").ToString());

            EmbaralharBaralho(PlayerPrefs.GetInt("seed"));
            PlayerPrefs.SetInt("cartaNumero", 1);

            SceneManager.LoadScene("TelaCarta");
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Dado : MonoBehaviour
{
    string str;
    // Start is called before the first frame update
    void Start()
    {
        Random.InitState(System.Environment.TickCount);
        int valor = Random.Range(1,7);
        str = valor.ToString();

        gameObject.GetComponent<TextMeshProUGUI>().text = str;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
